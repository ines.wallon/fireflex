#!/usr/bin/bash -e

FIREFLEX_ROOT="app"
PREFIX="${1:-/usr}"
APP_NAME="fireflex"
PRETTY_APP_NAME="FireFlex"

checkAppDependency() {
  CHECK="${*}"
  for CHECK do
    if ! [ -x "$(command -v "$CHECK")" ]; then
      echo "Error: $CHECK is not installed." >&2
      exit 1
    fi
  done
}

checkAppDependency pcregrep zenity getopt gtk-launch

install -d -m 755 "$PREFIX"/{bin,share/"$APP_NAME",share/"$APP_NAME"/lib}
install -m 755 "$FIREFLEX_ROOT/$APP_NAME" "$PREFIX/share/$APP_NAME"
install -m 755 "$FIREFLEX_ROOT/lib/helper.sh" "$PREFIX/share/$APP_NAME/lib"
ln -srf "$PREFIX/share/$APP_NAME/$APP_NAME" "$PREFIX/bin/$APP_NAME"
sed -i "s;/usr/bin/fireflex;$PREFIX/bin/$APP_NAME;g" $FIREFLEX_ROOT/$APP_NAME.desktop

desktop-file-install --rebuild-mime-info-cache --dir="$PREFIX/share/applications" "$FIREFLEX_ROOT/$APP_NAME.desktop"

echo "Congratulation $PRETTY_APP_NAME is Installed"
echo "Don't forget to set $PRETTY_APP_NAME to default browser"
exit 0
