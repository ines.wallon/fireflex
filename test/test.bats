#!/usr/bin/env bats

setup() {
    load 'test_helper/bats-support/load'
    load 'test_helper/bats-assert/load'
    load 'test_helper/bats-files/load'
    DIR="$( cd "$( dirname "$BATS_TEST_FILENAME" )" >/dev/null 2>&1 && pwd )"
    PATH="$DIR/../app:$DIR/test_helper:$PATH"
    APP="declare -a CONF_DIR=(\"$DIR/../docs/example/\"); . $DIR/../app/fireflex"
}

@test "Check if URL exists in Firefox" {
    run bash -c "$APP -l DEBUG -d https://google.com/"
    assert_output --partial 'Run "https://google.com/" with firefox'
}

@test "Check if URL exists in another browser" {
    run bash -c "$APP -l DEBUG -d https://meet.jit.si/test"
    assert_output --partial 'Run "https://meet.jit.si/test" with jitsi'
}

@test "Check if URL doesn't exist in config files" {
    run bash -c "$APP -l DEBUG -d https://titi.com/"
    assert_output --partial "Browser selected get choice"

}
