MAKEFLAGS += --silent
DOCKER_BUILD_IMAGE = "registry.famillewallon.com/docker-images/fedora-build"
default: help

.PHONY: help
help:  ## Display this help.
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)


.PHONY: test
test: ## Run unit test.
	bats test/test.bats

.PHONY: quality
quality: ## Run shellcheck quality tool.
	shellcheck -x -a app/fireflex app/lib/*.sh

.PHONY: build-rpm
build-rpm:  ## Build rpm
	@sudo dnf builddep -y fireflex.spec
	@spectool --define "_topdir $$(pwd)/rpmbuild" --define "_sourcedir $$(pwd)" -g -R fireflex.spec
	@rpmbuild --define "_topdir $$(pwd)/rpmbuild" --define "_sourcedir $$(pwd)" -bp fireflex.spec
	@rpmbuild --define "_topdir $$(pwd)/rpmbuild" --define "_sourcedir $$(pwd)" -bi fireflex.spec
	@rpmbuild --define "_topdir $$(pwd)/rpmbuild" --define "_sourcedir $$(pwd)" -ba fireflex.spec

.PHONY: docker-build-rpm
docker-build-rpm: ## Build rpm inside docker container.
	@docker pull $(DOCKER_BUILD_IMAGE)
	@docker run -it -v "$$(pwd)":/project -w /project $(DOCKER_BUILD_IMAGE) make build-rpm

# https://stackoverflow.com/a/6273809/1826109
%:
	@:
