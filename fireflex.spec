Name:           fireflex
Version:        1.0.1
Release:        1%{?dist}
Summary:        Choose your browser

License:        GPLv3
URL:            https://gitlab.com/ines.wallon/fireflex/
Source:         https://gitlab.com/ines.wallon/fireflex/-/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires:       desktop-file-utils

Requires:       gtk3
Requires:       bash
Requires:       pcre-tools
Requires:       util-linux
Requires:       zenity

%description
Fireflex gives you the opportunity to choose the web browser of your choice when you click on a URL

%prep
%setup

%install
#rm -rf ${RPM_BUILD_ROOT}
install -d ${RPM_BUILD_ROOT}%{_datadir}/%{name}/lib
install -d ${RPM_BUILD_ROOT}%{_datadir}/applications/
install -d ${RPM_BUILD_ROOT}%{_bindir}/
install -m 644 app/lib/helper.sh ${RPM_BUILD_ROOT}/%{_datadir}/%{name}/lib/helper.sh
install -m 644 app/%{name}.desktop ${RPM_BUILD_ROOT}/%{_datadir}/applications/%{name}.desktop
install -m 775 app/%{name} ${RPM_BUILD_ROOT}/%{_datadir}/%{name}/%{name}
ln -srf ${RPM_BUILD_ROOT}/%{_datadir}/%{name}/%{name} ${RPM_BUILD_ROOT}%{_bindir}

desktop-file-install \
 --rebuild-mime-info-cache \
 --dir=${RPM_BUILD_ROOT}%{_datadir}/applications \
 ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop

%files
%license LICENSE
%{_datadir}/%{name}/lib/helper.sh
%{_datadir}/%{name}/%{name}
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop


%changelog
* Thu Feb 02 2023 Ines <ines+fireflex@famillewallon.com>
- Init file
