#!/bin/bash

NC='\033[0m'
RED='\033[00;31m'
#GREEN='\033[00;32m'
YELLOW='\033[00;33m'
BLUE='\033[0;34m'

# Logging functions
function log_output {
  echo -ne "$(date '+%Y/%m/%d %H:%M:%S') $1\n"
}

function log_debug {
  if [[ "$LOGLEVEL" =~ ^(DEBUG)$ ]]; then
   log_output "$BLUE [DEBUG] ${FUNCNAME[1]} : $1 $NC"
  fi
}

function log_info {
  if [[ "$LOGLEVEL" =~ ^(DEBUG|INFO)$ ]]; then
    log_output "${YELLOW}[INFO] :${NC}  $1"
  fi
}

function log_warn {
  if [[ "$LOGLEVEL" =~ ^(DEBUG|INFO|WARN)$ ]]; then
    log_output "${YELLOW}[WARN]${NC}  ${FUNCNAME[1]} : $1"
  fi
}

function log_error {
  if [[ "$LOGLEVEL" =~ ^(DEBUG|INFO|WARN|ERROR)$ ]]; then
    log_output "${RED}[ERROR]${NC} ${FUNCNAME[1]} : $1 "
  fi
}

# Help output
function usage {
  echo "$PRETTY_APP_NAME version $VERSION"
  echo -e "\nUsage: $0 [options] [url]\n"
  echo -e "Options:\n"
  echo "  --help, -h        This help."
  echo "  --loglevel, -l    DEBUG, INFO, WARN, ERROR."
  echo -e "  --dry-run, -d     Dry-run without launch browser.\n"
  exit 1
}
