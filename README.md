# Fireflex

Fireflex allows you to choose the web browser you want when you want to open a URL.

![](docs/demo.mp4)

## Prerequisites

* gtk3
* pcre-tools
* util-linux
* zenity

## Installation

### Fedora
Use [FireFLex Copr Repository](https://copr.fedorainfracloud.org/coprs/missd/FireFlex/)

```bash
dnf copr enable missd/FireFlex
dnf install fireflex -y
# Set FireFlex to default browser
xdg-settings set default-web-browser fireflex.desktop 
```

### Manual installation
```bash
git clone https://gitlab.com/ines.wallon/fireflex.git
cd fireflex
sudo ./install.sh
# Set FireFlex to default browser
xdg-settings set default-web-browser fireflex.desktop
```

## How to automatically select browser for a website?

* Create config directory
```bash
mkdir ~/.config/fireflex/ # OR for all users /etc/fireflex 
```
* Create a browser conf file (e.g. google-chrome.conf) in `~/.config/fireflex/` conf directory and add URL into this file.
See [docs/example](docs/example) for more exemple.
